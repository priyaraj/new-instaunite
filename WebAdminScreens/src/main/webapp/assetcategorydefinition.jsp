<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@page import="com.mongodb.MongoClient"%>
 <%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.DBCollection"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBRemoteRentalDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Instaunite | Admin_I</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>




</head>
<script type="text/javascript">




function validateForm(newasset){
	
	
	<%
	MongoClient mongo = (MongoClient) request.getServletContext()
	.getAttribute("MONGO_CLIENT");
String databaseName = (String) request.getServletContext()
	.getAttribute("PONNIVI_REMOTERENTAL_DATABASE");

DB ponniviRemoteRentalDb = mongo.getDB(databaseName);  //Here this is Ponnivi_RemoteRentaldb database as we are checking in that only

session.setAttribute("remoterentaldb", ponniviRemoteRentalDb);
DBCollection assetCategoryTypeCollection = ponniviRemoteRentalDb.getCollection("Asset_Category_Type");
List equipmentTypeList = MongoDBRemoteRentalDAO.getEquipmentDetails(assetCategoryTypeCollection,0,"Asset_Category_Id","Asset_Category_Type", "");
if (equipmentTypeList != null) {
	session.setAttribute("equipmentTypes", equipmentTypeList);
	System.out.println("equipmentTypeList.."+equipmentTypeList);
}
%>
	 if(!newasset.form.newassetcategory.checked){
	
	<% if (equipmentTypeList == null) { %>
	alert("No asset category still exists. Select New Asset Category checkbox for adding Asset Category");
  	return false;
	<%} %>
	if (newasset.value == "Add Prices") {
	//checking for existence of assets
	<% MongoDBRemoteRentalDAO.getAssetsList(ponniviRemoteRentalDb, "Assets", session);
	 List assetsList = (List)session.getAttribute("assetsList");
	if (assetsList == null) { %>
		alert("No assets still exists. Click Add Assets and then Add Prices");
	  	return false;
	<%}
	 %>
	}
	}
	 
	 
	if(newasset.form.newassetcategory.checked) {
	var assetcategorytype = document.getElementById("assetcategorytype").value; 
	var assetcategorydescription = document.getElementById("assetcategorydescription").value; 
	if (newasset.value == "Add Prices") {
		//checking for existence of assets
		<% MongoDBRemoteRentalDAO.getAssetsList(ponniviRemoteRentalDb, "Assets", session);
		 assetsList = (List)session.getAttribute("assetsList");
		if (assetsList == null) { %>
		alert("No assets still exists. Click Add Assets and then Add Prices");
		  	return false;
		<%}
		 %>
		}
	 
	
	if (assetcategorytype==null || assetcategorytype=="")
	  {
	  	alert("Assetcategory Type should not be empty");
	  	return false;
	  }
	  
	  if (assetcategorydescription==null || assetcategorydescription=="")
	  {
	  	alert("Assetcategory Description should not be empty");
	  	return false;
	  }
	}

}

var checkDisplay = function(newassetcategory, form) { //check ID, form ID
	form = document.getElementById(form), newasset = document.getElementById(newassetcategory);
	
	   newasset.onclick = function(){
		form.style.display = (this.checked) ? "block" : "none";
		form.reset();
		
	};
	newasset.onclick();
};



</script>


<body>

   <div id="logo">
      <img src="Images/instaunite_logo.png"/>
      </div>
	   	<h1>Asset Category Details</h1>
         <hr>
 <form name="assetcategory" action="setAssetCategoryDetails" method="post">  	
	
     <div id="tablediv" align="center" >
          <table id="new1" class="tblcls_2" align="center" style="width: 100%;padding-left:0px;  ">
          <tr>
    	       		    <%
    if(request.getAttribute("success")!=null){
%>
 <p1 style="color:green"><%= request.getAttribute("success") %>  </p1>
<%
}
%>
</tr>
    	
    	<tr>
    	       		    <%
    if(request.getAttribute("error")!=null){
%>
 <p id="errormsg" style="color:red"><%= request.getAttribute("error") %>  </p>
<%
}
%>
</tr>
<tr>
<td id="guide" style="width: 100%;float:left"><h6>Click on New Asset Category to add New Asset Categories or else click on Submit to add New Assets</h6></td>
</tr>

			<table id="asset_cat" >
			
			<td id="align_left"><h11> New Asset Category  &nbsp;&nbsp;&nbsp;&nbsp;</h11> 
			<input type="checkbox" style="float:left" id="newassetcategory" name="newassetcategory"  />
			</td>
			</tr>
			<tr>
			
			</tr>
			</table>
		<table id="form" >
		<tbody style="float:left;">
		<tr>
		  <td id="align_left"><h11> Category Type</h11></td>
		    <td id="align_left" ><input type="textbox"  name="assetcategorytype" id="assetcategorytype"/></td>
		 </tr>
		 <tr>
		  <td id="align_left"><h11> Category Description</h11></td>
		    <td id="align_left"><input type="textbox"  name="assetcategorydescription" id="assetcategorydescription"/></td>
		 </tr>
		 </tbody>
		  </table> 
</table>
</div>

 <table id="addbtntbl">
 <tr>
 <td id="addassetcat">  <input type="submit" name="submit" id="submit" class="sbtbtnadd" value="Add AssetCategories" onclick="return(validateForm(this));" /></td>
 <td id="addasset">  <input type="submit" name="submit" id="submit" class="sbtbtnaddasset" value="Add Assets"  onclick="return(validateForm(this));"/></td>
 <td id="addassetpri">  <input type="submit" name="submit" id="submit" class="sbtbtnaddprice" value="Add Prices"  onclick="return(validateForm(this));"/></td>
 </tr>
 </table>
               
   <script type="text/javascript">
	checkDisplay("newassetcategory", "form");
	</script>          
         
            
           </form>

</body>
</html>