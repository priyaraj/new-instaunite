<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>    
<%@page import="org.json.JSONObject"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Instaunite | Admin_AssetPricing</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/admin.css"/>
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
<script type="text/javascript" src="js/reusable.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

</head>
<script type="text/javascript">
jQuery(document).ready(function($){
    $(document).on("keypress", "form", function(event) { 
        return event.keyCode != 13;
    });
    $(document).keydown(function (e) {

    	   if (e.which == 9){
    	            //alert("tab press");
    		   var assetName = document.getElementById("assetid").value;
    	    	//alert("checkAssetAvailability" + assetName);
    	    	var usage = document.getElementById("usage").value;
    	    	var currency = document.getElementById("currency").value;
    	    	//alert("checkAssetAvailability" + assetCategoryId);
    	    	//alert("runFancy.." + runFancy);
    	    		var browser=get_browser_info();
    			//alert("browser.."+browser.name + ".." + browser.version);
    			browserName = browser.name;
    	    	checkIfAvailable('Asset Name',assetName,'Usage ',usage,'Currency',currency,document.pricingform,browserName);
    	     }
    	     
    	    });
});
function checkPriceAvailability(e){
	
    if(e.keyCode == 13 || e.keyCode == 9){    	
    	var assetName = document.getElementById("assetid").value;
    	//alert("checkAssetAvailability" + assetName);
    	var usage = document.getElementById("usage").value;
    	var currency = document.getElementById("currency").value;
    	//alert("checkAssetAvailability" + assetCategoryId);
    	//alert("runFancy.." + runFancy);
    		var browser=get_browser_info();
		//alert("browser.."+browser.name + ".." + browser.version);
		browserName = browser.name;
    	checkIfAvailable('Asset Name',assetName,'Usage ',usage,'Currency',currency,document.pricingform,browserName);
    }
}
function initializePredefinedValues()
{
	<%
	List currenciesList = MongoDBDAO.getCurrencies();
	if (!currenciesList.isEmpty()) {
		%>
		var currenciesListOptions = '<select id="currency" name="currency">';
		<%		
	     for(int i = 0; i < currenciesList.size();i++)
	     { 
	    	 if (!currenciesList.get(i).toString().equalsIgnoreCase("")) {
	     %>
	     currenciesListOptions += '<option><%= currenciesList.get(i) %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	currenciesListOptions += '</select>';
	document.getElementById('currency').outerHTML = currenciesListOptions;
	<%
	List wholeAssetsList 	= new ArrayList();
	wholeAssetsList = (ArrayList)session.getAttribute("assetsList");
	if (!wholeAssetsList.isEmpty()) {
		%>
		var wholeAssetsListOptions = '<select id="assetid" name="assetid">';
		<%		
	     for(int i = 0; i < wholeAssetsList.size();i++)
	     { 
	    	 if (!wholeAssetsList.get(i).toString().equalsIgnoreCase("")) {
	    		 JSONObject jsonObject = (JSONObject)wholeAssetsList.get(i);
	     %>
	     wholeAssetsListOptions += '<option value="<%= jsonObject.get("id") %>"><%= jsonObject.get("description") %></option>';	
	     <% 
	    	 }
	     }		
	}
	%>
	wholeAssetsListOptions += '</select>';
	document.getElementById('assetid').outerHTML = wholeAssetsListOptions;
	
	

}
function validateForm()
  {
  
  var price = document.getElementById("price").value;

  
  if (price==null || price=="")
  {
  	alert("price should not be empty");
  	return false;
  }
  else {
		if (isNaN(price)) 
		 {
		   alert("Must input numbers for Price");
		   return false;
		 }
	}
  }
</script>

<body onload = "initializePredefinedValues()">
 <div id="logo">
      <img src="Images/instaunite_logo.png"/>
      </div>
	   	<h1>Pricing Details</h1>
         <hr>
<form name = "pricingform" action="setPricingDetails" method="post">         
<table id="pricingtable" align="center">
<tr>
    	       		    <%
    if(request.getAttribute("error")!=null){
%>
 <p id="errormsg" style="color:red"><%= request.getAttribute("error") %>  </p>
<%
}
%>
</tr>
<tr >
<td><h10>Asset Id </h10></td>
<td>
                	<select name="assetid" id="assetid">
					
					</select>
     
</td>
</tr>
<tr>
<td><h10>Usage </h10></td>
<td><select name="usage" id = "usage">
     <option value="24 hours">24 hours</option>
     <option value="8 hours per day">8 hours per day</option>
     <option value="Weekdays only">Weekdays only</option>
     <option value="Weekend only">Weekend only</option>
     <option value="4 hours per day">4 hours per day</option>
     <option value="no limit">no limit</option>
     <option value="monthly anytime">monthly anytime</option>
     </select></td>
</tr>
<tr>
<td><h10>Price</h10></td>
<td><input type="number"  name="price" id="price" min="1" onkeypress="checkPriceAvailability(event)"/></td>
</tr>
<tr>
<td><h10>Currency Type</h10></td>
<td><select name="currency" id="currency">
					</select></td>
</tr>
</table>
<table id="btnprice">
               
       <td> <input type="submit" name="submit" id="submit" class="sbtbtn_assetpriceadd" value="Add prices" onclick="return(validateForm());" /></td>     
       <td> <input type="submit" name="submit" id="submit" class="sbtbtn_assetprice" value="Submit"  onclick="return(validateForm());"/></td>

</table>
</form>
</body>
</html>