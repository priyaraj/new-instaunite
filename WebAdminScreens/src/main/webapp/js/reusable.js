function get_browser_info(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE',version:(tem[1]||'')};
        }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
 }
 

function checkIfAvailable(param1Name,param1Value,param2Name,param2Value,param3Name,param3Value,form,browserName) {
	//alert("in checkAvailability.." + runfancy);
if (param1Name == "Asset Name" && param2Name == "" && param3Name == "") {
	if (param1Value == null || param1Value =="")
   {
		alert(param1Name + " Should not be empty");
		return false;
   }
}
else {

	var xmlhttp;
	
	var urls="checkIfExisting.jsp?param1="+param1Value + "&param2="+param2Value + "&param3="+param3Value;
	//alert("urls" + urls);
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  //alert("urls in if.."+urls);
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	 // alert("urls in else.."+urls);
	  }
	
		xmlhttp.onreadystatechange=function()  {
		//alert("inside onreadystatechange" + xmlhttp.readyState);
	  if (xmlhttp.readyState == 4 )
	    {

		  
	  }
		}  // onreadystatechange
		
		xmlhttp.onload = function() {
			//alert(xmlhttp.responseText + ".." + xmlhttp.responseText.length);
			if (form.name == "pricingform") {
				
				if ( xmlhttp.responseText.length > 11 ) {
					alert("Data already exists. Hence values will be updated instead of inserting.");		
				}
			}
			
			if (form.name == "assetsform") {
			//alert("length="+xmlhttp.responseText.length);
			if ( xmlhttp.responseText.length > 15 ) {
				alert("Data already exists. Hence values will be updated instead of inserting.");
            var xmlDoc = new DOMParser().parseFromString(xmlhttp.responseText,'text/xml');

            console.log(xmlDoc);
            var x=xmlDoc.getElementsByTagName("assets");
            // alert(x.length);
             if (x.length > 0) {   //when there are values for asset_names
             for (i=0;i<x.length;i++)
             { 
             	form.assetdescription.value = x[i].getElementsByTagName("Asset_Description")[0].childNodes[0].nodeValue;
             	form.assetavail.value = x[i].getElementsByTagName("rental_availability")[0].childNodes[0].nodeValue;
             	if (browserName == "IE" || browserName == "MSIE") {
             		//alert("from ie" + x[i].getElementsByTagName("country")[0].childNodes[0].nodeValue);
             		var selectedValue = x[i].getElementsByTagName("country")[0].childNodes[0].nodeValue;
             		form.country.options[0].text = selectedValue;
				form.country.options[0].value = selectedValue;	
				//form.categoryCapacity.options[0].value = x[i].getElementsByTagName("category_capacity")[0].childNodes[0].nodeValue;
             	}
             	else {
                 	form.country.value = x[i].getElementsByTagName("country")[0].childNodes[0].nodeValue;
                 	
             	}
             	form.availfrm.value = x[i].getElementsByTagName("availability_from")[0].childNodes[0].nodeValue;
             	//alert(x[i].getElementsByTagName("endcustomerpostalcode")[0].childNodes[0].nodeValue);
             	form.year.value = x[i].getElementsByTagName("year_of_making")[0].childNodes[0].nodeValue;
             	form.maintenance.value = x[i].getElementsByTagName("last_maintenace")[0].childNodes[0].nodeValue;
             	form.fault.value = x[i].getElementsByTagName("last_major_fault")[0].childNodes[0].nodeValue;
             	
             	form.powersupply.value = x[i].getElementsByTagName("power_supply")[0].childNodes[0].nodeValue;
             	form.dieselCapacity.value = x[i].getElementsByTagName("diesel_capacity")[0].childNodes[0].nodeValue;
             	form.categoryCapacity.value = x[i].getElementsByTagName("category_capacity")[0].childNodes[0].nodeValue;
             }
             }
			}
             else {
             	clearFields(form,browserName);
             }
		}
		}
       
	xmlhttp.open("GET",urls,true);
	xmlhttp.send();
	}
	
}
function clearFields(form,browserName) {	
	//alert(form.name);
	form.assetdescription.value = "";
	form.assetavail.value = "YES";	
	if (browserName == "IE" || browserName == "MSIE") {
		form.country.options[0].text = "Afghanistan";
	}
	else {
		form.country.value = "Afghanistan";
	}
	
	form.availfrm.value = "";
	form.year.value = "";
	form.maintenance.value = "";
	
	form.fault.value = "";
	form.powersupply.value = "YES";
	form.dieselCapacity.value = "100 litres";
	form.categoryCapacity.value = "weight based";
	}
