<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Instaunite | Admin_I</title>

<link rel="stylesheet" type="text/css" href="css/style.css"  />
<link rel="stylesheet" type="text/css" href="css/admin.css"/>
<link rel="stylesheet" type="text/css" href="css/style-responsive.css"/>
</head>
<script type="text/javascript">
function validateForm()
{
var x=document.getElementById("customerId").value;
if (x==null || x=="")
{
	alert("Customer ID must be filled out");
	return false;
}
if(x.substring(0,2) != "CU")
{
	alert("Customer ID should starts with CU");
	return false;
}
}
</script>


<body>

   <div id="logo">
      <img src="Images/instaunite_logo.png"/>
      </div>
	   	<h1>Internal Representation of customer data</h1>
         <hr>
 <form action="getCustomerDetails" method="post">  	
	
     <div id="tablediv" align="center">
          <table class="tblcls_2" align="center">
          <tr>
    	       		    <%
    if(request.getAttribute("success")!=null){
%>
 <p2 style="color:green"><%= request.getAttribute("success") %>  </p2>
<%
}
%>
</tr>
    	<tr>
    	       		    <%
    if(request.getAttribute("error")!=null){
%>
 <p2 style="color:red"><%= request.getAttribute("error") %>  </p2>
<%
}
%>
</tr>
    	<tr>
        	<td>
            	<div id="custidtle">
                	<h9>*</h9><p>  Assign Customer ID </p>
        		</div>
             </td>
             <td >
             </td>
 			<td>
            	 <div id="custid_1" align="center">
                 	<input type="text" class="txtbox"  id="customerId" value="" name="customerId" />
                  </div>   
            </td>
       		<td>
       		     <div id="set" align="center">
               		<input type="button" class="setupbtn" onclick="validateForm();" value="Set"/>
       			</div>
       		</td>
  
		</tr>
        <tr>
        	<td>
            	<div id="custidtle">
                	<p> Assign Asset ID notation </p>
                 </div>
             </td>
             <td >
             </td>
             <td>
             	 <div id="custid_1" align="center">
                 	<input type="text" class="txtbox" value="CUxxx" />
                  </div> 
              </td>
              <td>
              	 <div id="set" align="center">
               		<input type="button" class="setupbtn" value="Set"/>
       			</div>
                </td>
         </tr>
          <tr>
        	<td>
            	<div id="custidtle">
                	<p> Assign SE ID notation </p>
                 </div>
             </td>
             <td >
             </td>
             <td>
             	 <div id="custid_1" align="center">
                 	<input type="text" class="txtbox" value="CUxxx" />
                  </div> 
              </td>
              <td>
               <div id="set" align="center">
               		<input type="button" class="setupbtn" value="Set"/>
       			</div>
                </td>
         </tr>
         <tr>
        	<td>
            	<div id="custidtle">
                	<p> Solution Category </p>
                 </div>
            </td>
            <td >
             </td>
            <td>
            	<div id="dropmenu" align="center">
                	<select name="appTypes" id="appTypes">
					<option value="Telecom Service Engineer">Telecom Service Engineer</option>
					<option value="Remote Evidence Management">Remote Evidence Management</option>
					<option value="Machine Supervisor">Machine Supervisor</option>
					<option value="Customer Service Forms">Customer Service Forms</option>
					<option value="Asset Tracking">Asset Tracking</option>
					<option value="Service Round Management">Service Round Management</option>
					<option value="Remote Rental Management">Remote Rental Management</option>
					</select>
                 </div>
            </td>
           <td>
           </td>
           </tr>
           <tr>
           		<td>
            	<div id="custidtle">
                	<p> Remote Unit names </p>
                 </div>
             </td>
             <td >
             </td>
             <td>
             	 <div id="custid_1" align="center">
                 	<input type="text" class="txtbox" value="CUxxx" />
                  </div> 
              </td>
              <td>
               <div id="set" align="center">
               		<input type="button" class="setupbtn" value="Upload"/>
       			</div>
                </td>
         	</tr>
             <tr>
           		<td>
            	<div id="custidtle">
                	<p> Serving people names </p>
                 </div>
             </td>
             <td >
             </td>
             <td>
             	 <div id="custid_1" align="center">
                 	<input type="text" class="txtbox" value="CUxxx" />
                  </div> 
              </td>
              <td>
              	<div id="set" align="center">
               		<input type="button" class="setupbtn" value="Upload"/>
       			</div>
                </td>
         </tr>
          
      </table>
      </div>
       
       	<table class ="table2" align="center">
        	<tr>
            	<td><div id="custidtle_pass">
                	<p> Customer username pwds </p>
                    </div>
            	 </td>
            	<td>
           			<div id="generate" align="center">
                      		<input type="button" class="generate" value="Generate"/>
       				</div>
           		</td>
              </tr>
              <tr>
            	<td><div id="custidtle_pass">
                	<p> Customer asset tags </p>
                    </div>
            	 </td>
            	<td>
           			<div id="generate" align="center">
                      		<input type="button" class="generate" value="Generate"/>
       				</div>
            		
           		</td>
              </tr>
              
         </table>
                
         	<table class ="table3" align="center">
        	 <tr>
           		<td>
            	<div id="custidtle_upl">
                	<p> Upload customer<br> relavent files </p>
                 </div>
             </td>
             <td>
             	 <div id="custid_2" align="center">
                 	<input type="text" class="txtbox" value="CUxxx" />
                  </div> 
              </td>
              <td>&nbsp;&nbsp;
                </td>
              <td>
         <div id="set" align="center">
               		<input type="button" class="setupbtn" value="Upload"/>
       			</div>
                </td>
                
         	</tr>
         	 <tr>
             <td>
             </td>
             <td>
               <div id="submit" align="center">
                   <input type="submit" style="margin-left:0px;" class="sbtbtn" value="Submit"/>
           </div>
                </td>
             </tr>
             </table>
            
            
           </form>
</body>
</body>
</html>