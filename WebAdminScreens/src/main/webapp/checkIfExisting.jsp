<%@page import="com.ponnivi.mongodb.dao.MongoDBRemoteRentalDAO"%>
<%@page import="com.ponnivi.mongodb.model.AssetsDetails"%>
<%@page import="com.ponnivi.mongodb.model.PricingDetails"%>
<%@page import="com.mongodb.DBCollection"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.ponnivi.mongodb.dao.MongoDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mongodb.DB"%>
<%@page import="com.mongodb.MongoClient"%> 
<%@page import="com.mongodb.Mongo"%>
<%@page import="com.mongodb.DB"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%!
%>
<%
            response.setContentType("text/xml");
            String param1Value =request.getParameter("param1");
            System.out.println("param1Value.."+param1Value);
            String param2Value =request.getParameter("param2");
            System.out.println("param2Value.."+param2Value);
            String param3Value =request.getParameter("param3");
            System.out.println("param3Value.."+param3Value);
            String customerDbdatabaseName = (String)session.getAttribute("customerdbname");
			Mongo mongo = (MongoClient)session.getAttribute("mongo");
            DB db = mongo.getDB(customerDbdatabaseName);
            session.setAttribute("customerdb",db);  
            DBCollection collection = null;
           if (param3Value.equalsIgnoreCase("")) {  //Because for Pricing, we are passing third arguemnt
           	collection = db.getCollection("Assets");
           AssetsDetails assetsDetails = MongoDBRemoteRentalDAO.getDetails(collection, "Asset_Name", "text", param1Value, "Asset_Category_Id", "number", param2Value, "text", "Asset_Name");
           if (assetsDetails != null) {
           session.setAttribute("assetsDetails", assetsDetails);
           out.println("<assets>");
           
  		  out.println("<Asset_Name>"+ assetsDetails.getAssetName() + "</Asset_Name>");
  		  out.println("<Asset_Description>"+ assetsDetails.getAssetDescription() + "</Asset_Description>");
  		  
  		 out.println("<rental_availability>"+ assetsDetails.getRentalAvailability() + "</rental_availability>");
  		 out.println("<country>"+ assetsDetails.getCountry() + "</country>");
		out.println("<availability_from>"+ assetsDetails.getAvailabilityFrom() + "</availability_from>");
  		out.println("<year_of_making>"+ assetsDetails.getYearOfMaking() + "</year_of_making>");
		 out.println("<last_maintenace>"+ assetsDetails.getLastMaintenance() + "</last_maintenace>");
 		 out.println("<last_major_fault>"+ assetsDetails.getLastMajorFault() + "</last_major_fault>");
		 	out.println("<power_supply>"+ assetsDetails.getPowerSupply() + "</power_supply>");
 			 out.println("<diesel_capacity>"+ assetsDetails.getDieselCapacity() + "</diesel_capacity>");
 		 
 		out.println("<category_capacity>"+ assetsDetails.getCategoryCapacity() + "</category_capacity>");
  		  
  		  out.println("</assets>");
           
           }
           else {
      //     out.println("<assets>" + "   ");
        //   	out.println("</assets>");
           	}
           }
           else {
        	   db = (DB)session.getAttribute("remoterentaldb");
        	   collection = db.getCollection("Pricing");
        	   PricingDetails pricingDetails  = MongoDBRemoteRentalDAO.getPricingDetails(collection, "Asset_Id", "number", param1Value, "Usage", "text", param2Value, "Currency", "text", param3Value);
               if (pricingDetails != null) {
               session.setAttribute("pricingDetails", pricingDetails);
               out.println("<pricing>");               
               out.println("</pricing>");
               }
           }
           
			
           
			
 %>