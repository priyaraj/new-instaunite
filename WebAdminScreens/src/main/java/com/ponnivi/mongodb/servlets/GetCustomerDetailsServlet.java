package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.model.AssetCategoryTypeDetails;
import com.ponnivi.mongodb.model.CustomerDetails;

@WebServlet("/getCustomerDetails")
public class GetCustomerDetailsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String customerId = request.getParameter("customerId");
		String appTypes = request.getParameter("appTypes");
		// String password = request.getParameter("password");
		String redirectingJsp = "";
		boolean isCustomerIdExists = false;
		//boolean isValid = validateCredentials(customerId, "", request);
		CustomerDetails customerDetails = new CustomerDetails();
		MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute("MONGO_CLIENT");
		String databaseName = (String) request.getServletContext()
				.getAttribute("PONNIVI_DATABASE");
		DB ponniviDb = mongo.getDB(databaseName);  //Here this is ponnivi database as we are checking in that only
		HttpSession session = request.getSession();	
			List availableCustomerIdList = MongoDBDAO.getDistinct(ponniviDb,"customer_definition_collection",
					"CustomerId");
			if (availableCustomerIdList.size() == 0) { // first time insertion
				// insert into customer_definition_collection, prepare config
				// json
				isCustomerIdExists = false;
			} else {
				for (int i = 0; i < availableCustomerIdList.size(); i++) {
					if (availableCustomerIdList.get(i).toString()
							.equalsIgnoreCase(customerId)) {
											
						request.setAttribute("error",
								"CustomerId already exists. Please give different customer id");
						redirectingJsp = "/customerdefinition.jsp";
						isCustomerIdExists = true;
						break;
					}
				}
			}
				if (!isCustomerIdExists) {
					// insert into customer_definition_collection in ponnivi database
					customerDetails.setCustomerId(customerId);
					customerDetails.setAppTypes(appTypes);
					
					MongoDBDAO.setCustomerDefinition(ponniviDb,"customer_definition_collection",customerDetails);
					//create evidences_description_collection in customerdatabase. Here customerdatabase is "Ponnivi_" + customerid
					DB customerponniviDb = mongo.getDB("Ponnivi_"+customerDetails.getCustomerId());
					long collectionCount = MongoDBDAO.getCollectionCount("evidence_description_collection", customerponniviDb);
					if (collectionCount == 0) {
					MongoDBDAO.createEvidence_Description_Collection(customerponniviDb,customerDetails.getCustomerId(),customerDetails.getAppTypes());
					//report_param_collection is not required for Remote Rental Management. Hence we should skip here -- Sep 16
					if (!appTypes.equalsIgnoreCase("Remote Rental Management")) {
					//create report_param_collection
					collectionCount = MongoDBDAO.getCollectionCount("report_param_collection", customerponniviDb);
					if (collectionCount == 0) {
					String optionsArrayJsonFileName = MongoDBDAO.getJsonFileNames("options_array");
					MongoDBDAO optionsArrayDAO = new MongoDBDAO(customerponniviDb, "report_param_collection",optionsArrayJsonFileName);
					}
					}
					request.setAttribute("success",
							"Customer Definition is set. Customer DatabaseName is " + "Ponnivi_"+customerDetails.getCustomerId() + ". This will be used in appside");
					redirectingJsp = "/customerdefinition.jsp";
					}

					
				}

			
		else {

			redirectingJsp = "/customerdefinition.jsp";
		}

		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}

	private boolean validateCredentials(String userName, String password,
			HttpServletRequest request) {
		if ((userName == null || userName.equals(""))) {
			request.setAttribute("error", "Customer ID is Missing");
			return false;
		} /*
		 * else if ((password == null || password.equals(""))) {
		 * request.setAttribute("error", "Password is Missing"); return false; }
		 */
		return true;
	}
}
