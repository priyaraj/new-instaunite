package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.dao.MongoDBRemoteRentalDAO;
import com.ponnivi.mongodb.model.AssetCategoryTypeDetails;
import com.ponnivi.mongodb.model.AssetsDetails;
import com.ponnivi.mongodb.model.Countries;
import com.ponnivi.mongodb.model.PricingDetails;

@WebServlet("/setPricingDetails")
public class SavePricingServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String redirectingJsp = "";
		HttpSession session = request.getSession();
		
		
		boolean isPriceExistsAlready = false;
		DB ponniviRemoteRentalDb = (DB)session.getAttribute("remoterentaldb");
		DBCollection pricingCollection = ponniviRemoteRentalDb.getCollection("Pricing");
		String submit = (String)request.getParameter("submit");
		String assetId = (String)request.getParameter("assetid");
		String usage = (String)request.getParameter("usage");
		String price = (String)request.getParameter("price");
		String currency = (String)request.getParameter("currency");		
		//Before inserting into Pricing collection, check whether it is already inserted. asset_nameusage and currency combination is unique per assetcategoryid and hence if same combination is entered, we should not save in Pricing again, instead we should update
		PricingDetails pricingDetails = new PricingDetails();
		pricingDetails = new PricingDetails();
		
		pricingDetails.setAssetId(assetId);
		pricingDetails.setUsage(usage);
		pricingDetails.setCurrency(currency);
		pricingDetails.setRate(Double.parseDouble(price));
	PricingDetails alreadyExistedPricingDetails = (PricingDetails)session.getAttribute("pricingDetails");
	String collectionName = "Pricing";
	if (alreadyExistedPricingDetails != null && alreadyExistedPricingDetails.getAssetId().equalsIgnoreCase(assetId) &&
			alreadyExistedPricingDetails.getUsage().equalsIgnoreCase(usage) && alreadyExistedPricingDetails.getCurrency().equalsIgnoreCase(currency)) {
		MongoDBRemoteRentalDAO.updatePricing(ponniviRemoteRentalDb, collectionName, pricingDetails);			
	}
	else {  //save, fresh insert, here again check for existence as if user do not press enter key, then once again check need to be done here -- Oct 13
		DBCollection collection = ponniviRemoteRentalDb.getCollection(collectionName);
		alreadyExistedPricingDetails  = MongoDBRemoteRentalDAO.getPricingDetails(collection, "Asset_Id", "text", assetId, "Usage", "text", usage, "Currency", "text", currency);
		if (alreadyExistedPricingDetails != null) {
			MongoDBRemoteRentalDAO.updatePricing(ponniviRemoteRentalDb, collectionName, pricingDetails);
		}
		else {
			MongoDBRemoteRentalDAO.savePricing(ponniviRemoteRentalDb, collectionName, pricingDetails);
		}
	}
	session.setAttribute("pricingDetails",pricingDetails);
	session.setAttribute("success", "Pricing details inserted/updated successfully");

	if (submit.equalsIgnoreCase("Submit")) {
		redirectingJsp = "/thankyou.jsp";
	}
	else {
		redirectingJsp = "/assetpricing.jsp";
	}
		
		
	/*	PricingDetails pricingDetails  = MongoDBRemoteRentalDAO.getPricingDetails(pricingCollection, "Asset_Id", "number", assetId, "Usage", "text", usage, "Currency", "text", currency);
			if (pricingDetails != null) { // data is there already for chosen assetid, usage and currency combination
				// insert into Asset_Category_Type
				isPriceExistsAlready = true;
			} else {
				isPriceExistsAlready = false;		

			}
			//insert into Pricing Collection
				if (!isPriceExistsAlready) {
					pricingDetails = new PricingDetails();
					
					pricingDetails.setAssetId(assetId);
					pricingDetails.setUsage(usage);
					pricingDetails.setCurrency(currency);
					pricingDetails.setRate(Double.parseDouble(price));
					
					MongoDBRemoteRentalDAO.savePricing(ponniviRemoteRentalDb, collectionName, pricingDetails);					
					if (submit.equalsIgnoreCase("Submit")) {
						redirectingJsp = "/thankyou.jsp";
					}
					else {
						redirectingJsp = "/assetpricing.jsp";
					}
					
					}
		else {
			request.setAttribute("error",
					"For the given combination of Asset,Usage and Currency data already exists. Please give different combinations");
			redirectingJsp = "/assetpricing.jsp";
		}*/
		


		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	
	}

}
