package com.ponnivi.mongodb.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.dao.MongoDBRemoteRentalDAO;
import com.ponnivi.mongodb.model.AssetsDetails;

@WebServlet("/setAssetsDetails")
public class SaveAssetsServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	final static Logger logger = Logger.getLogger(SaveAssetsServlet.class);
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {		
		String redirectingJsp = "";
		HttpSession session = request.getSession();
		//Changes done on Nov 16. I.e. keeping Assets under Customer db instead of Ponnivi_RemoteRentaldb
		//DB db = (DB)session.getAttribute("remoterentaldb");
		DB db = (DB)session.getAttribute("ponnividb");
		String submit = (String)request.getParameter("submit");
		System.out.println("submit.." + submit);
		//Get the selected customer id here
		String customerId = (String)request.getParameter("customer");
		//String customerDbName = MongoDBDAO.getCollectionSpecificDetails(db,"customer_definition_collection","CustomerId",customerId);
		Mongo mongo = (MongoClient)session.getAttribute("mongo");
		//db = mongo.getDB(customerDbName);  //From here, Assets will get stored under specific customer eg : Ponnivi_CU_R01
		db = (DB)session.getAttribute("customerdb");  //From here, Assets will get stored under specific customer eg : Ponnivi_CU_R01

		String assetCategoryId = (String)request.getParameter("assetcategorytype");
		String assetName = (String)request.getParameter("assetname");
		String assetDescription = (String)request.getParameter("assetdescription");
		String rentalAvailability = (String)request.getParameter("assetavail");
		
		String country = (String)request.getParameter("country");
		String availabilityFrom = (String)request.getParameter("availfrm");
		String yearOfMaking = (String)request.getParameter("year");
		String lastMaintenance = (String)request.getParameter("maintenance");
		
		String lastMajorFault = (String)request.getParameter("fault");
		String powerSupply = (String)request.getParameter("powersupply");
		String dieselCapacity = (String)request.getParameter("dieselCapacity");
		String categoryCapacity = (String)request.getParameter("categoryCapacity");
		
		AssetsDetails assetsDetails = new AssetsDetails();
		//set customerId -- Nov 16
		assetsDetails.setCustomerId(customerId);
		assetsDetails.setAssetCategoryId(assetCategoryId);
		assetsDetails.setAssetName(assetName);
		assetsDetails.setAssetDescription(assetDescription);
		
		assetsDetails.setRentalAvailability(rentalAvailability);
		assetsDetails.setCountry(country);
		assetsDetails.setAvailabilityFrom(availabilityFrom);
		
		assetsDetails.setYearOfMaking(yearOfMaking);
		assetsDetails.setLastMaintenance(lastMaintenance);
		assetsDetails.setLastMajorFault(lastMajorFault);
		
		assetsDetails.setPowerSupply(powerSupply);
		assetsDetails.setDieselCapacity(dieselCapacity);
		assetsDetails.setCategoryCapacity(categoryCapacity);
		
		String collectionName = "Assets";
		boolean existsAlready = false;
		//Before inserting into Assets collection, check whether it is already inserted. asset_name is unique per assetcategoryid and hence if same asset_name is entered, we should not save in Assets again, instead we should update
		AssetsDetails alreadyExistedAssetsDetails = (AssetsDetails)session.getAttribute("assetsDetails");
		
		if (alreadyExistedAssetsDetails != null && alreadyExistedAssetsDetails.getAssetName().equalsIgnoreCase(assetName) &&
				alreadyExistedAssetsDetails.getAssetCategoryId().equalsIgnoreCase(assetCategoryId)) {
			MongoDBRemoteRentalDAO.updateAssets(db, collectionName, assetsDetails);			
		}
		else {  //save, fresh insert
		MongoDBRemoteRentalDAO.saveAssets(db, collectionName, assetsDetails);
		}
		session.setAttribute("assetsDetails",assetsDetails);
		/*List equipmentTypeList = (List)session.getAttribute("equipmentTypes");
		List assetsList = new ArrayList(),wholeAssetsList = new ArrayList();
		try {
		for (int i = 0; i < equipmentTypeList.size();i++) {
			JSONObject jsonObject = (JSONObject)equipmentTypeList.get(i);
			
				assetCategoryId = jsonObject.get("id").toString();
			
			DBCollection collection = db.getCollection(collectionName);
			assetsList = MongoDBRemoteRentalDAO.getEquipmentDetails(collection,Integer.parseInt(assetCategoryId),"Asset_Id","Asset_Name", "Asset_Id");
			if (assetsList != null) {
				wholeAssetsList.addAll(assetsList);
			}
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (wholeAssetsList != null && wholeAssetsList.size() > 0) {
			session.setAttribute("assetsList", wholeAssetsList);
		}*/
		MongoDBRemoteRentalDAO.getAssetsList(db, collectionName, session);
		if (submit.equalsIgnoreCase("Add other assets")) {
			redirectingJsp = "/assetsdefinition.jsp";
		}
		else {
			redirectingJsp = "/assetpricing.jsp";
		}
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}


}
