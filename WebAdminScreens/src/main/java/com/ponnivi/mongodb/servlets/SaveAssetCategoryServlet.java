package com.ponnivi.mongodb.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.ponnivi.mongodb.dao.MongoDBDAO;
import com.ponnivi.mongodb.dao.MongoDBRemoteRentalDAO;
import com.ponnivi.mongodb.model.AssetCategoryTypeDetails;
import com.ponnivi.mongodb.model.Countries;

import org.apache.log4j.Logger;

@WebServlet("/setAssetCategoryDetails")
public class SaveAssetCategoryServlet extends HttpServlet {

	private static final long serialVersionUID = -6554920927964049383L;
	final static Logger logger = Logger.getLogger(SaveAssetCategoryServlet.class);

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String redirectingJsp = "";
		HttpSession session = request.getSession();
		Countries counties = new Countries();
		String submit = (String)request.getParameter("submit");
		if(logger.isDebugEnabled()){
			logger.debug("Parameter selected : " + submit);
		}
		logger.warn("Parameter selected : " + submit);
		/*logger.error("This is error : " + submit);
		logger.fatal("This is fatal : " + submit);
		logger.info("This is info : " + submit);*/
		List countriesList = counties.countriesList;
		session.setAttribute("countriesList", countriesList);
		String newAssetCategory = request.getParameter("newassetcategory");

		DB ponniviRemoteRentalDb = (DB)session.getAttribute("remoterentaldb");
		DBCollection assetCategoryTypeCollection = ponniviRemoteRentalDb.getCollection("Asset_Category_Type");
		List equipmentTypeList = null; 
		
		if (newAssetCategory != null && newAssetCategory.equalsIgnoreCase("on")) { 
		String assetCategoryType = request.getParameter("assetcategorytype");
		String assetCategoryDescription = request.getParameter("assetcategorydescription");

		
		boolean isAssetCategoryTypeExists = false;

		AssetCategoryTypeDetails assetCategoryTypeDetails = new AssetCategoryTypeDetails();
		
		
			List availableAssetCategoryTypeList = MongoDBDAO.getDistinct(ponniviRemoteRentalDb,"Asset_Category_Type",
					"Asset_Category_Type");
			if (availableAssetCategoryTypeList.size() == 0) { // first time insertion
				// insert into Asset_Category_Type
				isAssetCategoryTypeExists = false;
			} else {
				for (int i = 0; i < availableAssetCategoryTypeList.size(); i++) {
					if (availableAssetCategoryTypeList.get(i).toString()
							.equalsIgnoreCase(assetCategoryType)) {											
						request.setAttribute("error",
								"Asset Category Type already exists. Please give different asset category type");
						redirectingJsp = "/assetcategorydefinition.jsp";
						isAssetCategoryTypeExists = true;
						break;
					}
				}
			}
				if (!isAssetCategoryTypeExists) {
					// insert into customer_definition_collection in ponnivi database
					assetCategoryTypeDetails.setAssetCategoryType(assetCategoryType);
					assetCategoryTypeDetails.setAssetCategoryDescription(assetCategoryDescription);
					
					MongoDBRemoteRentalDAO.saveAssetCategoryType(ponniviRemoteRentalDb, "Asset_Category_Type", assetCategoryTypeDetails);
					
					equipmentTypeList = MongoDBRemoteRentalDAO.getEquipmentDetails(assetCategoryTypeCollection,0,"Asset_Category_Id","Asset_Category_Type", "");
					if (equipmentTypeList != null) {
						session.setAttribute("equipmentTypes", equipmentTypeList);
					}
					if (submit.equalsIgnoreCase("Add AssetCategories")) {
						redirectingJsp = "/assetcategorydefinition.jsp";
					}
					else if (submit.equalsIgnoreCase("Add Assets")) {
						redirectingJsp = "/assetsdefinition.jsp";
					}
					else if (submit.equalsIgnoreCase("Add Prices")) {
						MongoDBRemoteRentalDAO.getAssetsList(ponniviRemoteRentalDb, "Assets", session);
						redirectingJsp = "/assetpricing.jsp";
					}
				}
		else {
			redirectingJsp = "/assetcategorydefinition.jsp";
		}
		}
		else {  // We are not inserting assetcategorytype but inserting into assets
			
			equipmentTypeList = MongoDBRemoteRentalDAO.getEquipmentDetails(assetCategoryTypeCollection,0,"Asset_Category_Id","Asset_Category_Type", "");
			if (equipmentTypeList != null) {
				session.setAttribute("equipmentTypes", equipmentTypeList);
				if (submit.equalsIgnoreCase("Add AssetCategories")) {
					redirectingJsp = "/assetcategorydefinition.jsp";
				}
				else if (submit.equalsIgnoreCase("Add Assets")) {
					redirectingJsp = "/assetsdefinition.jsp";
				}
				else if (submit.equalsIgnoreCase("Add Prices")) {
					MongoDBRemoteRentalDAO.getAssetsList(ponniviRemoteRentalDb, "Assets", session);
					redirectingJsp = "/assetpricing.jsp";
				}
			}
		}

		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				redirectingJsp);
		rd.forward(request, response);

	}


}
