/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014,2015  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> //Bean class that is used to pass as datasource : eg to rotating assets report

 * */
package com.ponnivi.mongodb.model;




public class CustomerDetails {
	public CustomerDetails(String customerId, String assetId,String seId,String appTypes,
			String remoteUnitNames,String servicePeopleNames) {
		super();
		this.customerId = customerId;
		this.assetId = assetId;
		this.seId = seId;
		this.appTypes = appTypes;
		this.remoteUnitNames = remoteUnitNames;
		this.servicePeopleNames = servicePeopleNames;
	}
	public CustomerDetails() {
		
	}

	private String customerId;
	private String assetId;
	private String seId	;
	private String appTypes;
	private String remoteUnitNames;
	private String servicePeopleNames;

	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getSeId() {
		return seId;
	}
	public void setSeId(String seId) {
		this.seId = seId;
	}
	public String getAppTypes() {
		return appTypes;
	}
	public void setAppTypes(String appTypes) {
		this.appTypes = appTypes;
	}
	public String getRemoteUnitNames() {
		return remoteUnitNames;
	}
	public void setRemoteUnitNames(String remoteUnitNames) {
		this.remoteUnitNames = remoteUnitNames;
	}
	public String getServicePeopleNames() {
		return servicePeopleNames;
	}
	public void setServicePeopleNames(String servicePeopleNames) {
		this.servicePeopleNames = servicePeopleNames;
	}
	
}