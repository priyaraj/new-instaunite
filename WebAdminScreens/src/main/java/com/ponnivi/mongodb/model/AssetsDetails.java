/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014,2015  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> //Bean class that is used to pass as datasource : eg to rotating assets report

 * */
package com.ponnivi.mongodb.model;




public class AssetsDetails {
	//Added on Nov 16 to have the reference of customerid in the assets
	private String customerId;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	//
	public AssetsDetails() {
		
	}
	private String assetId;
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	private String assetCategoryId;
	private String assetName;
	private String assetDescription;
	private String rentalAvailability;
	private String country;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	private String availabilityFrom;
	private String yearOfMaking;
	private String lastMaintenance;
	private String lastMajorFault;

	private String powerSupply;
	private String dieselCapacity;
	private String categoryCapacity;
	public String getAssetCategoryId() {
		return assetCategoryId;
	}
	public void setAssetCategoryId(String assetCategoryId) {
		this.assetCategoryId = assetCategoryId;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getAssetDescription() {
		return assetDescription;
	}
	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}
	public String getRentalAvailability() {
		return rentalAvailability;
	}
	public void setRentalAvailability(String rentalAvailability) {
		this.rentalAvailability = rentalAvailability;
	}
	public String getAvailabilityFrom() {
		return availabilityFrom;
	}
	public void setAvailabilityFrom(String availabilityFrom) {
		this.availabilityFrom = availabilityFrom;
	}
	public String getYearOfMaking() {
		return yearOfMaking;
	}
	public void setYearOfMaking(String yearOfMaking) {
		this.yearOfMaking = yearOfMaking;
	}
	public String getLastMaintenance() {
		return lastMaintenance;
	}
	public void setLastMaintenance(String lastMaintenance) {
		this.lastMaintenance = lastMaintenance;
	}
	public String getLastMajorFault() {
		return lastMajorFault;
	}
	public void setLastMajorFault(String lastMajorFault) {
		this.lastMajorFault = lastMajorFault;
	}
	public String getPowerSupply() {
		return powerSupply;
	}
	public void setPowerSupply(String powerSupply) {
		this.powerSupply = powerSupply;
	}
	public String getDieselCapacity() {
		return dieselCapacity;
	}
	public void setDieselCapacity(String dieselCapacity) {
		this.dieselCapacity = dieselCapacity;
	}
	public String getCategoryCapacity() {
		return categoryCapacity;
	}
	public void setCategoryCapacity(String categoryCapacity) {
		this.categoryCapacity = categoryCapacity;
	}
	

}