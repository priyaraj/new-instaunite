/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2014,2015  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> //Bean class that is used to pass as datasource : eg to rotating assets report

 * */
package com.ponnivi.mongodb.model;




public class AssetCategoryTypeDetails {
	public AssetCategoryTypeDetails(String assetCategoryType, String assetCategoryDescription) {
		super();
		this.assetCategoryType = assetCategoryType;
		this.assetCategoryDescription = assetCategoryDescription;
	}
	public AssetCategoryTypeDetails() {
		
	}

	private String assetCategoryType;
	private String assetCategoryDescription;
	public String getAssetCategoryType() {
		return assetCategoryType;
	}
	public void setAssetCategoryType(String assetCategoryType) {
		this.assetCategoryType = assetCategoryType;
	}
	public String getAssetCategoryDescription() {
		return assetCategoryDescription;
	}
	public void setAssetCategoryDescription(String assetCategoryDescription) {
		this.assetCategoryDescription = assetCategoryDescription;
	}
}