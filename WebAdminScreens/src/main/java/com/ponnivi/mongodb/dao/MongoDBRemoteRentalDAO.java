package com.ponnivi.mongodb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.ponnivi.mongodb.model.AssetCategoryTypeDetails;
import com.ponnivi.mongodb.model.AssetsDetails;
import com.ponnivi.mongodb.model.PricingDetails;

//DAO class for different MongoDBRemoteRentalDAO CRUD operations
//take special note of "id" String to ObjectId conversion and vice versa
//also take note of "_id" key for primary key
public class MongoDBRemoteRentalDAO extends MongoDBDAO {
	final static Logger logger = Logger.getLogger(MongoDBRemoteRentalDAO.class);
	
	public MongoDBRemoteRentalDAO(DB ponniviDb, String collectionName,
			String filePath) {
		super(ponniviDb, collectionName, filePath);
		// TODO Auto-generated constructor stub
	}

	//Get AssetsList
	public static void getAssetsList(DB db,
			String collectionName,HttpSession session) {
	List equipmentTypeList = (List)session.getAttribute("equipmentTypes");
	List assetsList = new ArrayList(),wholeAssetsList = new ArrayList();
	String assetCategoryId = "";
	if (equipmentTypeList != null) {
	try {
	for (int i = 0; i < equipmentTypeList.size();i++) {
		JSONObject jsonObject = (JSONObject)equipmentTypeList.get(i);
		
			assetCategoryId = jsonObject.get("id").toString();
		
		DBCollection collection = db.getCollection(collectionName);
		assetsList = MongoDBRemoteRentalDAO.getEquipmentDetails(collection,Integer.parseInt(assetCategoryId),"Asset_Id","Asset_Name", "Asset_Id");
		if (assetsList != null) {
			wholeAssetsList.addAll(assetsList);
		}
	}
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		logger.error("JSONException at getAssetsList.."+e.getMessage());
		//e.printStackTrace();
	}
	if (wholeAssetsList != null && wholeAssetsList.size() > 0) {
		session.setAttribute("assetsList", wholeAssetsList);
	}
	}
}
	
	//insert into Asset_Category_Type
	public static void saveAssetCategoryType(DB ponniviDb,
			String collectionName,AssetCategoryTypeDetails assetCategoryTypeDetails) {
		
		//ponniviDb = mongo.getDB(dbName);

		DBCollection collection = ponniviDb.getCollection(collectionName);
		String assetCategoryId  = getMaxValue("Asset_Category_Id",collection);
		if (assetCategoryId.equalsIgnoreCase("")) {
			assetCategoryId = "1";
		}
		else {
			assetCategoryId = String.valueOf(Integer.parseInt(assetCategoryId) + 1);
		}
		BasicDBObject assetCategoryTypeDefinition = new BasicDBObject();
		assetCategoryTypeDefinition.put("Asset_Category_Id", assetCategoryId);
		assetCategoryTypeDefinition.put("Asset_Category_Type", assetCategoryTypeDetails.getAssetCategoryType());
		assetCategoryTypeDefinition.put("Asset_Category_Description", assetCategoryTypeDetails.getAssetCategoryDescription());
		collection.insert(assetCategoryTypeDefinition);
		logger.warn("Inserted successfully into Asset_Category_Type collection");
	}
	
	//insert into Pricing
		public static void savePricing(DB ponniviDb,
				String collectionName,PricingDetails pricingDetails) {
			
			DBCollection collection = ponniviDb.getCollection(collectionName);
			String pricingId  = getMaxValue("Pricing_Id",collection);
			if (pricingId.equalsIgnoreCase("")) {
				pricingId = "1";
			}
			else {
				pricingId = String.valueOf(Integer.parseInt(pricingId) + 1);
			}
			BasicDBObject pricingDefinition = new BasicDBObject();
			pricingDefinition.put("Pricing_id", pricingId);
			pricingDefinition.put("Asset_Id", pricingDetails.getAssetId());
			pricingDefinition.put("Usage", pricingDetails.getUsage());
			pricingDefinition.put("Currency", pricingDetails.getCurrency());
			pricingDefinition.put("Rate", pricingDetails.getRate());
			collection.insert(pricingDefinition);			
			logger.warn("Inserted successfully into Pricing collection");
		}
	
	public static void saveAssets(DB db,
			String collectionName,AssetsDetails assetsDetails) {
		
		DBCollection collection = db.getCollection(collectionName);
		String assetId  = getMaxValue("Asset_Id",collection);
		//Changing into the notation type as customer_id_A_0001
		if (assetId.equalsIgnoreCase("")) {
			assetId = assetsDetails.getCustomerId() + "_A_" + "0001";
		}
		else {
			if (Integer.parseInt(assetId) < 10) {
			assetId = assetsDetails.getCustomerId() + "_A_000" + String.valueOf(Integer.parseInt(assetId) + 1);
			}
			else {
				assetId = assetsDetails.getCustomerId() + "_A_00" + String.valueOf(Integer.parseInt(assetId) + 1);
			}
		}
		BasicDBObject assetsDefinition = new BasicDBObject();
		assetsDefinition.put("Asset_Id", assetId);
		assetsDefinition.put("Asset_Category_Id", Integer.parseInt(assetsDetails.getAssetCategoryId()));
		assetsDefinition.put("Asset_Name", assetsDetails.getAssetName());
		
		assetsDefinition.put("Asset_Description", assetsDetails.getAssetDescription());
		assetsDefinition.put("rental_availability", assetsDetails.getRentalAvailability());
		assetsDefinition.put("country", assetsDetails.getCountry());
		assetsDefinition.put("availability_from", assetsDetails.getAvailabilityFrom());
		
		assetsDefinition.put("year_of_making", assetsDetails.getYearOfMaking());
		assetsDefinition.put("last_maintenace", assetsDetails.getLastMaintenance());
		assetsDefinition.put("last_major_fault", assetsDetails.getLastMajorFault());
		
		assetsDefinition.put("power_supply", assetsDetails.getPowerSupply());
		assetsDefinition.put("diesel_capacity", assetsDetails.getDieselCapacity());
		assetsDefinition.put("category_capacity", assetsDetails.getCategoryCapacity());
		
		collection.insert(assetsDefinition);
		logger.warn("Inserted successfully into Assets collection");
	}
	
	public static void updatePricing(DB db,
			String collectionName,PricingDetails pricingDetails) {
		BasicDBObject searchObject1 = new BasicDBObject();
		BasicDBObject searchObject2 = new BasicDBObject();
		BasicDBObject searchObject3 = new BasicDBObject();
		searchObject1.put("Asset_Id", Integer.parseInt(pricingDetails.getAssetId()));
		searchObject2.put("Usage", pricingDetails.getUsage());
		searchObject3.put("Currency", pricingDetails.getCurrency());
		BasicDBList queryList = new BasicDBList();
		queryList.add(searchObject1);
		queryList.add(searchObject2);
		queryList.add(searchObject3);
		DBObject query = new BasicDBObject("$and",queryList);
		
		DBObject updateFields = new BasicDBObject();
		DBCollection collection = db.getCollection(collectionName);
		updateFields = new BasicDBObject().append("Rate", pricingDetails.getRate());
		DBObject update = new BasicDBObject("$set", updateFields);
		collection.updateMulti(query, update);
		logger.warn("Pricing collection updated successfully");
		
	}
	
	public static void updateAssets(DB db,
			String collectionName,AssetsDetails assetsDetails) {
		BasicDBObject searchObject1 = new BasicDBObject();
		BasicDBObject searchObject2 = new BasicDBObject();
		searchObject1.put("Asset_Name", assetsDetails.getAssetName());
		searchObject2.put("Asset_Category_Id", Integer.parseInt(assetsDetails.getAssetCategoryId()));
		BasicDBList queryList = new BasicDBList();
		queryList.add(searchObject1);
		queryList.add(searchObject2);
		DBObject query = new BasicDBObject("$and",queryList);
		
		DBObject updateFields = new BasicDBObject();
		DBCollection collection = db.getCollection(collectionName);
		updateFields = new BasicDBObject().append("Asset_Description", assetsDetails.getAssetDescription())
				.append("rental_availability", assetsDetails.getRentalAvailability())
				.append("country", assetsDetails.getCountry())
				.append("availability_from", assetsDetails.getAvailabilityFrom())
				.append("year_of_making", assetsDetails.getYearOfMaking())
				.append("last_maintenace", assetsDetails.getLastMaintenance())
				.append("last_major_fault", assetsDetails.getLastMajorFault())
				.append("power_supply", assetsDetails.getPowerSupply())
				.append("diesel_capacity", assetsDetails.getDieselCapacity())
				.append("category_capacity", assetsDetails.getCategoryCapacity());
		DBObject update = new BasicDBObject("$set", updateFields);
		
		
		collection.updateMulti(query, update);
		
		logger.warn("Assets collection updated successfully");
	}
	
	public static List getEquipmentDetails(DBCollection collection,int categoryId,String fieldName1,String fieldName2, String fieldName3) {
		List equipmentTypeList = null;
		DBCursor cursor = null;
		BasicDBObject searchObject = new BasicDBObject();
		if (categoryId == 0) {
		cursor = collection.find();
		}
		else {
			searchObject.put("Asset_Category_Id",
					categoryId);
			cursor = collection.find(searchObject);
		}

		if (cursor.count() > 0) {
			equipmentTypeList = new ArrayList();
		while (cursor.hasNext()) {
			DBObject o = cursor.next();
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("id",o.get(fieldName1));
				jsonObject.put("description",o.get(fieldName2));
				if (!fieldName3.equalsIgnoreCase("")) {
					jsonObject.put("asset_id",o.get(fieldName3));
				}
				equipmentTypeList.add(jsonObject);
			} catch (JSONException e) {
				logger.error("JSON Exception at getEquipmentDetails"+e.getMessage());
				//e.printStackTrace();
			}		

		}
		}

		return equipmentTypeList;
	}

	public static AssetsDetails getDetails(DBCollection collection,String fieldName1,String field1DataType, String fieldValue1, String fieldName2, String field2DataType, String fieldValue2, String outputFieldDataType, String outputField) {
		String output = "";
		DBCursor cursor = null;
		BasicDBObject searchObject1 = new BasicDBObject();
		BasicDBObject searchObject2 = new BasicDBObject();
		BasicDBList resultList = new BasicDBList();
		if (field1DataType.equalsIgnoreCase("number")) {
			searchObject1.put(fieldName1,
					(int)Double.parseDouble(fieldValue1));
		}
		else {
			searchObject1.put(fieldName1,
					fieldValue1);
		}
		if (field2DataType.equalsIgnoreCase("number")) {
			searchObject2.put(fieldName2,
					(int)Double.parseDouble(fieldValue2));
		}
		else {
			searchObject2.put(fieldName2,
					fieldValue2);
		}

			resultList.add(searchObject1);
			resultList.add(searchObject2);
			DBObject query = new BasicDBObject("$and", resultList);
			cursor = collection.find(query);
		
			AssetsDetails assetsDetails = null;
		if (cursor.count() > 0) {
		while (cursor.hasNext()) {
			assetsDetails = new AssetsDetails();
			DBObject object = cursor.next();
			assetsDetails.setAssetId(object.get("Asset_Id" ).toString());
			assetsDetails.setAssetCategoryId(object.get("Asset_Category_Id").toString());
			assetsDetails.setAssetName(object.get("Asset_Name").toString());
			assetsDetails.setAssetDescription(object.get("Asset_Description").toString());
			
			assetsDetails.setRentalAvailability(object.get("rental_availability" ).toString());
			assetsDetails.setCountry(object.get("country").toString());			
			assetsDetails.setAvailabilityFrom(object.get("availability_from").toString());
			assetsDetails.setYearOfMaking(object.get("year_of_making").toString());
			
			assetsDetails.setLastMaintenance(object.get("last_maintenace").toString());
			assetsDetails.setLastMajorFault(object.get("last_major_fault").toString());
			assetsDetails.setPowerSupply(object.get("power_supply").toString());
			assetsDetails.setDieselCapacity(object.get("diesel_capacity").toString());
			assetsDetails.setCategoryCapacity(object.get("category_capacity").toString());
		}
		}

		return assetsDetails;
	}
	
	public static PricingDetails getPricingDetails(DBCollection collection,String fieldName1,String field1DataType, String fieldValue1, String fieldName2, String field2DataType, String fieldValue2, String fieldName3, String field3DataType, String fieldValue3) {
		String output = "";
		DBCursor cursor = null;
		BasicDBObject searchObject1 = new BasicDBObject();
		BasicDBObject searchObject2 = new BasicDBObject();
		BasicDBObject searchObject3 = new BasicDBObject();
		BasicDBList resultList = new BasicDBList();
		if (field1DataType.equalsIgnoreCase("number")) {
			searchObject1.put(fieldName1,
					(int)Double.parseDouble(fieldValue1));
		}
		else {
			searchObject1.put(fieldName1,
					fieldValue1);
		}
		if (field2DataType.equalsIgnoreCase("number")) {
			searchObject2.put(fieldName2,
					(int)Double.parseDouble(fieldValue2));
		}
		else {
			searchObject2.put(fieldName2,
					fieldValue2);
		}
		
		if (field3DataType.equalsIgnoreCase("number")) {
			searchObject3.put(fieldName3,
					(int)Double.parseDouble(fieldValue3));
		}
		else {
			searchObject3.put(fieldName3,
					fieldValue3);
		}

			resultList.add(searchObject1);
			resultList.add(searchObject2);
			resultList.add(searchObject3);
			DBObject query = new BasicDBObject("$and", resultList);
			cursor = collection.find(query);
		
			PricingDetails pricingDetails = null;
		if (cursor.count() > 0) {
		while (cursor.hasNext()) {
			pricingDetails = new PricingDetails();
			DBObject object = cursor.next();
			pricingDetails.setAssetId(object.get("Asset_Id" ).toString());
			pricingDetails.setUsage(object.get("Usage").toString());
			pricingDetails.setCurrency(object.get("Currency").toString());			
		}
		}

		return pricingDetails;
	}
	
	

}
