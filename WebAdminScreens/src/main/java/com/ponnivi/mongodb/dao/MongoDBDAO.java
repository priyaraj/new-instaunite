package com.ponnivi.mongodb.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.ponnivi.mongodb.model.AssetCategoryTypeDetails;
import com.ponnivi.mongodb.model.AssetsDetails;
import com.ponnivi.mongodb.model.CustomerDetails;

//DAO class for different MongoDB CRUD operations
//take special note of "id" String to ObjectId conversion and vice versa
//also take note of "_id" key for primary key
public class MongoDBDAO {


	public MongoDBDAO(DB ponniviDb,
			String collectionName, String filePath) {
		// System.out.println("filePath.." + filePath);
		//ponniviDb = mongo.getDB(dbName);

		DBCollection Collection = ponniviDb.getCollection(collectionName);

		// Get collectionName details and put

		// convert JSON to DBObject directly

		DBObject jsonDbObject = (DBObject) JSON.parse(getJsonDetails(filePath));

		Collection.insert(jsonDbObject);

	}
	public static String getMaxValue(String fieldName,DBCollection collection) {
		String maxValue = "";
		AggregationOutput output = null;
		DBObject groupFields = new BasicDBObject("_id",fieldName);
	    groupFields.put("times", new BasicDBObject("$sum",1));
	    DBObject group = new BasicDBObject("$group", groupFields);
	    DBObject sort = new BasicDBObject("$sort", new BasicDBObject("times",-1) );
	    DBObject limit = new BasicDBObject("$limit", 1 );		    
	    output = collection.aggregate(group, sort);

	    for (DBObject result : output.results()) {
			  System.out.println(result.get("_id") + ".." + result.get("times"));
			  maxValue = result.get("times").toString();
			  }
	    return maxValue;
	}
	//Read and parse the json data
	public static String getJsonDetails(String filePath) {
		FileReader reader;
		JSONObject jsonObject = null;
	
		try {
			
			reader = new FileReader(filePath);	 	 
         JSONParser jsonParser = new JSONParser();
         jsonObject = (JSONObject) jsonParser.parse(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         return jsonObject.toString();
	}

	
	// Get distinct customerId from customer_definition_collection or other distinct fields
	public static List getDistinct(DB ponniviDb,
			String collectionName, String distinctFieldName)
			throws UnknownHostException {

		//ponniviDb = mongo.getDB(dbName);

		DBCollection Collection = ponniviDb.getCollection(collectionName);

		// call distinct method and store the result in list l1
		List distinctList = Collection.distinct(distinctFieldName);

		// iterate through the list and print the elements
		for (int i = 0; i < distinctList.size(); i++) {
			System.out.println(distinctList.get(i));
		}

		return distinctList;

	}
	
	
	
	//insert into customer_definition_collection
	public static void setCustomerDefinition(DB ponniviDb,
			String collectionName,CustomerDetails customerDetails) {
		
		//ponniviDb = mongo.getDB(dbName);

		DBCollection collection = ponniviDb.getCollection(collectionName);
		BasicDBObject customerDefinition = new BasicDBObject();
		customerDefinition.put("CustomerId", customerDetails.getCustomerId());
		customerDefinition.put("assetId", customerDetails.getAssetId());
		customerDefinition.put("seId", customerDetails.getSeId());
		customerDefinition.put("appTypes", customerDetails.getAppTypes());
		customerDefinition.put("remoteUnitNames", customerDetails.getRemoteUnitNames());
		customerDefinition.put("servicePeopleNames", customerDetails.getServicePeopleNames());
		//Creation of dbname will be Ponnivi_customerid
		customerDefinition.put("CustomerDbName", "Ponnivi_" + customerDetails.getCustomerId());
		collection.insert(customerDefinition);
		
	}
	
	//insert into evidences_description_collection in customer databasename depends upon the apptype
	public static void createEvidence_Description_Collection(DB ponniviDb,
			String customerId,String appType) {
		//ponniviDb = mongo.getDB(dbName);
		if (ponniviDb != null) {
			   DBCollection collection = ponniviDb.getCollection("evidence_description_collection");
			   Map<String, Object> evidenceRecordMap = new HashMap<String, Object>();		   
			   
				
			   List<Map<String, Object>> itemsList = new ArrayList<Map<String,Object>>();
				Map<String, Object> evidenceDescriptionItemsMap = new HashMap<String, Object>();
				if (appType.equalsIgnoreCase("Asset Tracking")) {
					evidenceDescriptionItemsMap.put("A", "Art.nr");
					evidenceDescriptionItemsMap.put("B", "Produkt");
					evidenceDescriptionItemsMap.put("C", "price");
					evidenceDescriptionItemsMap.put("D","Incharge");
				}
				else if (appType.equalsIgnoreCase("Remote Rental Management")) {
					evidenceDescriptionItemsMap.put("A", "Equipment Name");
					evidenceDescriptionItemsMap.put("B", "Usage Description");
					evidenceDescriptionItemsMap.put("C", "Quantity");
					evidenceDescriptionItemsMap.put("D","No of days");
					
				}
				else {
					evidenceDescriptionItemsMap.put("A", "null");
					evidenceDescriptionItemsMap.put("B", "null");
					evidenceDescriptionItemsMap.put("C", "null");
					evidenceDescriptionItemsMap.put("D","null");
				}
				
				itemsList.add(evidenceDescriptionItemsMap);
			 
				//Here changes are required as per appTypes
				Map<String, Object> evidenceDescriptionEvidencesMap = new HashMap<String, Object>();
				evidenceDescriptionEvidencesMap.put("Evidence_1", "Image_Directory_Location");
				if (appType.equalsIgnoreCase("Asset Tracking")) {
				evidenceDescriptionEvidencesMap.put("Evidence_2", "Service_Note");
				evidenceDescriptionEvidencesMap.put("Evidence_3", "Missing_Note");
				evidenceDescriptionEvidencesMap.put("Evidence_4", "Scanned_Status");
				evidenceDescriptionEvidencesMap.put("Evidence_5", "Added_Item");
				
				evidenceDescriptionEvidencesMap.put("Evidence_6", "Deleted_Item");
				}
				if (appType.equalsIgnoreCase("Machine Supervisor")) {
					evidenceDescriptionEvidencesMap.put("Evidence_2", "Reasons");
					evidenceDescriptionEvidencesMap.put("Evidence_3", "Machine DownTime");
					evidenceDescriptionEvidencesMap.put("Evidence_4", "Action");
					evidenceDescriptionEvidencesMap.put("Evidence_5", "null");
					
					evidenceDescriptionEvidencesMap.put("Evidence_6", "null");					
				}
				if (appType.equalsIgnoreCase("Telecom Service Engineer")) {
					evidenceDescriptionEvidencesMap.put("Evidence_2", "Service_Note");
					evidenceDescriptionEvidencesMap.put("Evidence_3", "Missing_Note");
					evidenceDescriptionEvidencesMap.put("Evidence_4", "Choose_Part");
					evidenceDescriptionEvidencesMap.put("Evidence_5", "null");
					
					evidenceDescriptionEvidencesMap.put("Evidence_6", "null");					
				}
				if (appType.equalsIgnoreCase("Remote Rental Management")) {
					evidenceDescriptionEvidencesMap.put("Evidence_2", "Mileage");
					evidenceDescriptionEvidencesMap.put("Evidence_3", "Set Alarm");
					evidenceDescriptionEvidencesMap.put("Evidence_4", "Set Notifications");
					evidenceDescriptionEvidencesMap.put("Evidence_5", "Additional Notifications");
					
					evidenceDescriptionEvidencesMap.put("Evidence_6", "null");					
				}
				//For rest of apps, we need to add -- June 29
				evidenceDescriptionEvidencesMap.put("Evidence_7", "null");
				evidenceDescriptionEvidencesMap.put("Evidence_8", "null");
				evidenceDescriptionEvidencesMap.put("Evidence_9", "null");
				evidenceDescriptionEvidencesMap.put("Evidence_10", "null");
				itemsList.add(evidenceDescriptionEvidencesMap);
				
				Map<String, Object> evidenceDescriptionQualifiersMap = new HashMap<String, Object>();
				if (appType.equalsIgnoreCase("Asset Tracking") || appType.equalsIgnoreCase("Telecom Service Engineer")) {
				evidenceDescriptionQualifiersMap.put("Qualifier_1", "To_Service_Item");
				evidenceDescriptionQualifiersMap.put("Qualifier_2", "Missing_Item");
				evidenceDescriptionQualifiersMap.put("Qualifier_8", "Category");
				}
				if (appType.equalsIgnoreCase("Machine Supervisor")) {
					evidenceDescriptionQualifiersMap.put("Qualifier_1", "unknown machine issue");
					evidenceDescriptionQualifiersMap.put("Qualifier_2", "null");
					evidenceDescriptionQualifiersMap.put("Qualifier_8", "null");					
				}
				if (appType.equalsIgnoreCase("Remote Rental Management")) {
					evidenceDescriptionQualifiersMap.put("Qualifier_1", "Milege");
					evidenceDescriptionQualifiersMap.put("Qualifier_2", "manual/automatic switch on and off");
				}
				//Qualifier_3
				if (appType.equalsIgnoreCase("Telecom Service Engineer")) {
					evidenceDescriptionQualifiersMap.put("Qualifier_3", "partname");
				}
				else if (appType.equalsIgnoreCase("Remote Rental Management")) {
					evidenceDescriptionQualifiersMap.put("Qualifier_3", "email/phone");					
				}
				else {
				evidenceDescriptionQualifiersMap.put("Qualifier_3", "null");
				}

				if (appType.equalsIgnoreCase("Remote Rental Management")) {
					evidenceDescriptionQualifiersMap.put("Qualifier_4", "email/phone");
					evidenceDescriptionQualifiersMap.put("Qualifier_8", "null");
				}
				else {
					evidenceDescriptionQualifiersMap.put("Qualifier_4", "null");
				}
				
				evidenceDescriptionQualifiersMap.put("Qualifier_5", "null");
				
				evidenceDescriptionQualifiersMap.put("Qualifier_6", "null");
				evidenceDescriptionQualifiersMap.put("Qualifier_7", "null");
				
				evidenceDescriptionQualifiersMap.put("Qualifier_9", "null");
				evidenceDescriptionQualifiersMap.put("Qualifier_10", "null");
				itemsList.add(evidenceDescriptionQualifiersMap);
				
				Map<String, Object> evidenceDescriptionChildMap = new HashMap<String, Object>();
				evidenceDescriptionChildMap.put("Items", itemsList);
				
				//context_reference_example
				if (appType.equalsIgnoreCase("Asset Tracking")) {
				evidenceDescriptionChildMap.put("Context_Reference_Example", "Rotating_assets");
				}
				if (appType.equalsIgnoreCase("Telecom Service Engineer")) {
					evidenceDescriptionChildMap.put("Context_Reference_Example", "Telecom_Tower_Assets");
				}
				if (appType.equalsIgnoreCase("Machine Supervisor")) {
				evidenceDescriptionChildMap.put("Context_Reference_Example", "machine_uptime");
				}
				//If required we may need to change context reference example
				if (appType.equalsIgnoreCase("Remote Evidence Management")) {
					evidenceDescriptionChildMap.put("Context_Reference_Example", "Remote_Evidence_Management");
				}
				if (appType.equalsIgnoreCase("Customer Service Forms")) {
					evidenceDescriptionChildMap.put("Context_Reference_Example", "Customer Service Forms");
				}

				if (appType.equalsIgnoreCase("Service Round Management")) {
					evidenceDescriptionChildMap.put("Context_Reference_Example", "Service Round Management");
				}
				//For Remote rental -- Added on Sep 16
				if (appType.equalsIgnoreCase("Remote Rental Management")) {
					evidenceDescriptionChildMap.put("Context_Reference_Example", "Remote_Rental_Assets");
				}
				//
				//asset_xxx,yyy,zzz
				if (appType.equalsIgnoreCase("Machine Supervisor")) {
					evidenceDescriptionChildMap.put("Asset_Xxx", "Machine Name");
					evidenceDescriptionChildMap.put("Asset_Yyy", "Supervisor Name");
					evidenceDescriptionChildMap.put("Asset_Zzz", "RemoteUnit Name");
				}
				else if (appType.equalsIgnoreCase("Remote Rental Management")) {
				evidenceDescriptionChildMap.put("Asset_Xxx", "subscriber name");
				evidenceDescriptionChildMap.put("Asset_Yyy", "order id");
				evidenceDescriptionChildMap.put("Asset_Zzz", "");
				}
				else {
					evidenceDescriptionChildMap.put("Asset_Xxx", "null");
					evidenceDescriptionChildMap.put("Asset_Yyy", "null");
					evidenceDescriptionChildMap.put("Asset_Zzz", "null");
				}
				
				//
				evidenceDescriptionChildMap.put("CustomerId", customerId);
				evidenceDescriptionChildMap.put("CustomerDbName", "Ponnivi_"+customerId);
				
				evidenceDescriptionChildMap.put("Number_Of_Items", "67");  //Here the count will be differed by means of uploaded json values
				if (appType.equalsIgnoreCase("Asset Tracking") || appType.equalsIgnoreCase("Remote Rental Management")) {
				evidenceDescriptionChildMap.put("Multuple_Items" , "yes");
				}
				else {  //If required we need change here
					evidenceDescriptionChildMap.put("Multuple_Items" , "no");
				}
				evidenceDescriptionChildMap.put("Evidence_Identifier" , "manual_entry");
				//
				evidenceDescriptionChildMap.put("Context_Type", "existing context");
				evidenceDescriptionChildMap.put("Partial_Entries_Allowed", "no");
				evidenceDescriptionChildMap.put("Evidence_Location", "gps");
				if (appType.equalsIgnoreCase("Asset Tracking")) {
				evidenceDescriptionChildMap.put("Number_Of_Evidences_In_Each_Item","6");
				}
				else if (appType.equalsIgnoreCase("Machine Supervisor")) {
					evidenceDescriptionChildMap.put("Number_Of_Evidences_In_Each_Item","4");  //Here also there will be change
				}
				else if (appType.equalsIgnoreCase("Remote Rental Management")) {
					evidenceDescriptionChildMap.put("Number_Of_Evidences_In_Each_Item","5");  //Here also there will be change
				}
				
				//"Evidence_Location_Entries"
				Map<String, Object> evidenceLocationMap = new HashMap<String, Object>();
				evidenceLocationMap.put("Entry1", "latitude");
				evidenceLocationMap.put("Entry2", "longtitude");
				evidenceLocationMap.put("Entry3", "null");
				evidenceLocationMap.put("Entry4", "null");
				evidenceLocationMap.put("Entry5", "null");
				evidenceDescriptionChildMap.put("Evidence_Location_Entries", evidenceLocationMap);
				
				//capturing_device_details
				Map<String, Object> evidenceCapturingDetailsMap = new HashMap<String, Object>();
				evidenceCapturingDetailsMap.put("Screen_Size", "");
				evidenceCapturingDetailsMap.put("API", "");
				evidenceCapturingDetailsMap.put("Screen_Height", "");
				evidenceCapturingDetailsMap.put("Phonemodel", "");
				evidenceCapturingDetailsMap.put("xdpi", "");
				evidenceCapturingDetailsMap.put("Density", "");
				
				evidenceCapturingDetailsMap.put("ydpi", "");
				evidenceCapturingDetailsMap.put("Device_Id", "");
				evidenceCapturingDetailsMap.put("Phonemanufacturer", "");
				evidenceCapturingDetailsMap.put("Screen_Width", "");
				evidenceDescriptionChildMap.put("Capturing_Device_Details", evidenceCapturingDetailsMap);
				
				evidenceRecordMap.put("Evidence_Record", evidenceDescriptionChildMap);
				collection.insert(new BasicDBObject(evidenceRecordMap));
				
		   }
		
		
		
	}

	public static long getCollectionCount(String collectionName,DB ponniviDb) {
		//ponniviDb = mongo.getDB(dbName);
		long count = 0;
		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);

			count = collection.getCount();

		}
		return count;

	}
	
	public static String getJsonFileNames(String type) {
		String jsonFileName = "";
		String userOperatingSystem = System.getProperty("os.name");		
		if (type.equalsIgnoreCase("options_array")) {
			if (userOperatingSystem.startsWith("Windows")) {
				//jsonFileName = "D:\\EMS\\contactformsamples\\src\\main\\webapp\\options_array.json";				
				jsonFileName = "C:\\priya\\Reportsidecode\\reportsidecode\\contactformsamples\\src\\main\\webapp\\options_array.json";
			}
			else {
				jsonFileName = "/home/ubuntu/instaunite/androidapp/telecomassets/jsons/options_array.json";
			}
		}
		return jsonFileName;		
	}
	public static ArrayList<String> getCurrencies() {
	ArrayList<String> currencys = new ArrayList<String>();
    Locale[] locs = Locale.getAvailableLocales();

    for(Locale loc : locs) {
        try {
            String val=Currency.getInstance(loc).getCurrencyCode();
            if(!currencys.contains(val))
                currencys.add(val);
        } catch(Exception exc)
        {
            // Locale not found
        }
        Collections.sort(currencys);
    }
    return currencys;
	}
	//Added on Nov 16
	
	public static String getCollectionSpecificDetails(DB ponniviDb, String collectionName,String fieldName,String fieldValue) {
		String result = "";
	//	ponniviDb = mongo.getDB(dbName);
		if (ponniviDb != null) {
			DBCollection collection = ponniviDb.getCollection(collectionName);
			BasicDBObject searchObject = new BasicDBObject();
			searchObject.put(fieldName,
					fieldValue);
			DBCursor cursor = collection.find(searchObject);
			while (cursor.hasNext()) {
				// System.out.println(cursor.next());
				DBObject dbObject = cursor.next();
				if (collectionName.equalsIgnoreCase("customer_definition_collection")) {
					result = (String)dbObject.get("CustomerDbName");
				}
			}			
		}
		return result;
	}
}













